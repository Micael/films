from flask import jsonify, abort, make_response, request, url_for
import services.service as service
import os 
import pandas as pd
import controllers.films as films
def recommandations(id, serialize=True):
    recommandations = films.get_recommandations(id)
    if serialize :
        return jsonify(films=[film[0].serialize() for film in recommandations])
    else  : 
        return recommandations