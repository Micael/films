from flask import render_template
import middlewares.api as api

def page_index():
	return render_template('index.html')

def page_recommandations(id):
	films = api.recommandations(id, serialize=False)
	return render_template('recommandations.html', films=films)
