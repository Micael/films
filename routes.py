from flask import Flask, render_template
import middlewares.website as website
import middlewares.api as api

app = Flask(__name__)

# Fonction qui instancie les differentes Routes API
def get_routes_website(app):
    app.add_url_rule('/recommandations/<int:id>', 'page_index', website.page_recommandations, methods=['GET'])
    #app.add_url_rule('/api/users', 'add_users', users.create, methods=['POST'])
    #app.add_url_rule('/api/users/<int:id>', 'update_users', users.update, methods=['PUT'])
    #app.add_url_rule('/api/users/<int:id>', 'delete_userx', users.delete, methods=['DELETE'])


def get_routes_api(app):
    app.add_url_rule('/api/recommandations/<int:id>', 'get_films', api.recommandations, methods=['GET'])
# Fonction qui instancie les differentes Routes d'erreur
# def init_error_handlers(app):
#     app.errorhandler(404)(errors.handle_error_404)
#     app.errorhandler(500)(errors.handle_error_500)
