# coding: utf-8
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import StaticPool

from models import Films
from models import Avis
class FilmsService:

    def __init__(self, engine):
        if not engine:
            raise ValueError('The values specified in engine parameter has to be supported by SQLAlchemy')
        self.engine = engine
        db_engine = create_engine(engine,   connect_args={'check_same_thread': False},
                poolclass=StaticPool, echo=True)
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    # Candidat
    def add_films(self, titre):
        film = Films(titre)

        self.session.add(film)
        self.session.commit()

        return film.id

    def get_films(self, id=None, serialize=False):
        films = []

        if id is None:
            films = self.session.query(Films).order_by(Films.id).all()
        else:
            films = self.session.query(Films).filter(Films.id == id).all()

        if serialize:
            return [film.serialize() for film in films]
        else:
            return films

    def get_ratings(self, id=None, serialize=False):
        avis = []

        if id is None:
            avis = self.session.query(Avis).order_by(Avis.id).all()
        else:
            avis = self.session.query(Avis).filter(Avis.id == id).all()

        if serialize:
            return [a.serialize() for a in avis]
        else:
            return avis
            
    def import_films(self, films, avis):

        for film in films :
            self.session.add(Films(id=film[0], titre=film[1], genre=film[2]))
        
        self.session.commit()

        for a in avis :
            self.session.add(Avis(user=a[0], film=a[1], rating=float(a[2]), timestamp=a[3]))

        self.session.commit()
