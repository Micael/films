
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
from operator import itemgetter
import pandas as pd
from random import *
from math import *
import sys
import services.service as service
import csv
db_engine = 'sqlite:////home/kingdom/workspaces/films/db/db.db'

filmsservice = service.FilmsService(db_engine)
films = filmsservice.get_films()
labels = []
for film in films:
    genres = film.genre.split('|')
    labels = labels + list(set(genres) - set(labels))

rating = []
for label in labels: 
    tot = 0
    length = 0 
    for film in films :
        if label in film.genre.split('|')  :
            for avis in film.avis:
                tot = tot + avis.rating
                length = length + 1
        else :
            continue
    if tot > 0 :
        moyenne = round(tot / length, 1)
    else : 
        moyenne = 0 
    rating.append(moyenne)

names = list(labels)
values = list(rating)

data = pd.read_csv('./download/ml-latest-small/ratings.csv')

data.rating.value_counts().plot(kind='bar')
plt.show()

fig, axs = plt.subplots()

axs.bar(names, values)
fig.suptitle('Categorie films')
plt.show()

fig, axs = plt.subplots()

axs.scatter(names, values)
plt.show()
fig, axs = plt.subplots()

axs.plot(names, values)
plt.show()