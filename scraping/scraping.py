from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
import zipfile

def scrap():
    profile = webdriver.FirefoxProfile()
    profile.set_preference('browser.download.folderList', 2) # custom location
    profile.set_preference('browser.download.manager.showWhenStarting', False)
    profile.set_preference('browser.download.dir', os.getcwd() + '/download')
    profile.set_preference("browser.helperApps.neverAsk.openFile", 'application/zip')
    profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'application/zip')

    driver = webdriver.Firefox(firefox_profile=profile, executable_path='./geckodriver')

    driver.get("https://grouplens.org/datasets/movielens/")

    link = driver.find_element_by_link_text('ml-latest-small.zip')

    link.click()

    zip_ref = zipfile.ZipFile(os.getcwd() + str('/download/ml-latest-small.zip'), 'r')
    zip_ref.extractall(os.getcwd() + '/download')
    zip_ref.close()


    print(link)