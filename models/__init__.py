from models.Films import Films
from models.Avis import Avis
# Ce fichier permet d'importer tous les modeles en important juste le dossier
__all__ = [Films, Avis]
