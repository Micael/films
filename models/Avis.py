from sqlalchemy import Column, String, Integer, ForeignKey, Float,String
from sqlalchemy.orm import relationship
from models.Model import Model

class Avis(Model):

    __tablename__ = 'avis'

    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    user = Column(Integer, nullable=True)
    film = Column(Integer, ForeignKey('films.id'))
    rating = Column(Float, nullable=True)
    timestamp = Column(String, nullable=True)

    def serialize(self):
        return {
            "id": self.id,
            "user": self.user,
            "film": self.film,
            "rating": self.rating,
            "timestamp": self.timestamp
        }
