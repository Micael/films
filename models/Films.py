from sqlalchemy import Column, String, Integer, ForeignKey, Numeric, Date
from sqlalchemy.orm import relationship
from models.Model import Model

class Films(Model):

    __tablename__ = 'films'
    id = Column(Integer, primary_key=True, nullable=False,autoincrement=True)
    titre = Column(String(200), nullable=True)
    genre = Column(String(600), nullable=True)
    avis = relationship("Avis")

    def serialize(self):
        return {
            "id": self.id,
            "titre": self.titre,
            "genre": self.genre,
            "avis" : [a.serialize() for a in self.avis]
        }
