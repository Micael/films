# film
init db + scramping https://grouplens.org/datasets/movielens/
> python init.py


matplotlip 
> python matplotlib-films.py 

Surprise recommadations 
> python app.py

routes 
front > http://127.0.0.1:5000/recommandations/3
api > http://127.0.0.1:5000/api/recommandations/3

Reflection votre système de recommandation est performant ? 
on canstate que le système est assez performant car les films recommandés
ont des catégories similaires et qu'il filtre du meilleur au moin bien noté, ainsi que sur les bonnes et mauvaises predictions
bon predirection :
Estimating biases using als...
RMSE: 0.8614
       uid   iid  rui  est                     details   Iu   Ui  err
9647    43  1084  5.0  5.0  {u'was_impossible': False}   83   25  0.0
15132  452  1617  5.0  5.0  {u'was_impossible': False}  160   69  0.0
3898    43   364  5.0  5.0  {u'was_impossible': False}   83  124  0.0
1770   452   260  5.0  5.0  {u'was_impossible': False}  160  186  0.0
22291    1  2959  5.0  5.0  {u'was_impossible': False}  162  154  0.0
5129   122  2959  5.0  5.0  {u'was_impossible': False}  204  154  0.0
19773    1  1213  5.0  5.0  {u'was_impossible': False}  162   93  0.0
14257  452  1221  5.0  5.0  {u'was_impossible': False}  160   93  0.0
24069  452   608  5.0  5.0  {u'was_impossible': False}  160  128  0.0
16561  122  1136  5.0  5.0  {u'was_impossible': False}  204   92  0.0

mauvaise predirection : 
Estimating biases using als...
RMSE: 0.8576
       uid     iid  rui       est                     details   Iu   Ui       err
6841   125   55820  0.5  4.068593  {u'was_impossible': False}  272   48  3.568593
6162   393    5902  0.5  4.081265  {u'was_impossible': False}   87   30  3.581265
2959   258   87232  0.5  4.104422  {u'was_impossible': False}   16   33  3.604422
5838   580    1262  0.5  4.124981  {u'was_impossible': False}  335   34  3.624981
131    594    5909  0.5  4.202495  {u'was_impossible': False}  185    4  3.702495
20954  522  106100  0.5  4.209745  {u'was_impossible': False}  154   13  3.709745
3357   598     593  0.5  4.273787  {u'was_impossible': False}   16  206  3.773787
3855   573    3996  0.5  4.451732  {u'was_impossible': False}  214   91  3.951732
12351  256    7099  0.5  4.596923  {u'was_impossible': False}  126   14  4.096923
4919   256    5618  0.5  4.637541  {u'was_impossible': False}  126   64  4.137541