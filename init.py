# coding: utf-8

import csv
import config.init_db as init_db
import services.service as service
import os 
import pandas as pd
import scraping.scraping as scraping


def get_tab_movies():
    with open('./download/ml-latest-small/movies.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar=' ')
        tab_movies = []
        skip_first = 0

        for row in spamreader:
            if(skip_first == 0):
                skip_first = 1
                continue
            if(skip_first == 100): 
                break
            skip_first = skip_first + 1
            tab_movies.append(row)

    return tab_movies

def get_tab_ratings():
    with open('./download/ml-latest-small/ratings.csv') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar=' ')
        tab_ratings = []
        skip_first = 0
        for row in spamreader:
            if(skip_first == 0):
                skip_first = 1
                continue
            if(skip_first == 100) :
                break
            skip_first = skip_first + 1
            tab_ratings.append(row)
            
    return tab_ratings

scraping.scrap()

db_engine = 'sqlite:///' + os.getcwd() + '/db/db.db'
init_db.init_database(db_engine)
service = service.FilmsService(db_engine)
service.import_films(get_tab_movies(), get_tab_ratings())
