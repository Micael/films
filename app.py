from flask import Flask, render_template
import routes
import config.init_db as init_db
import controllers.films as films
import models.Films as Films
app = Flask(__name__)

routes.get_routes_api(app)
routes.get_routes_website(app)


if __name__ == "__main__":
	app.secret_key = 'super secret key'
	app.run(debug=True)